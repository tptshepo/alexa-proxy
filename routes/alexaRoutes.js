var express = require('express');

var routes = function (Alexa, AlexaResponse) {
    var alexaRouter = express.Router();

    var alexaController = require('../controllers/alexaController')(Alexa, AlexaResponse);
    
    alexaRouter.route('/')
        .post(alexaController.handleRequest);
    
    return alexaRouter;
};

module.exports = routes;