/// <reference path="typings/node/node.d.ts"/>

var express = require('express'),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    https = require('https'),
    fs = require('fs');

var db;

// configure certificates
var options = {
    key: fs.readFileSync('certs/private-key.pem'),
    cert: fs.readFileSync('certs/certificate.pem'),
    requestCert: false,
    rejectUnauthorized: false
};

db = mongoose.connect('mongodb://mgaga.co.za/echo');

var app = express();

//var port = process.env.PORT || 443;
var port = 443;

app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({ limit: '5mb', extended: true }));

// models
var Alexa = require('./models/alexa');
var AlexaResponse = require('./models/alexaResponse');

// routes
var alexaRouter = require('./routes/alexaRoutes')(Alexa, AlexaResponse);

// use route
app.use('/api/alexa', alexaRouter);

// home page
app.get('/', function (req, res) {
    res.send('Welcome to the echo proxy');
});

var server = https.createServer(options, app).listen(port, function () {
    console.log("Server started at port " + port);
});

module.exports = app;