var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var alexaResponseModel = new Schema({
    command: { type: String, required: true },
    response: { type: Object, required: true }
});

module.exports = mongoose.model('AlexaResponse', alexaResponseModel);