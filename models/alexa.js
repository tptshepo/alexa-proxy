var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var alexaModel = new Schema({
    payload: { type: Object, required: true },
    createDate: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Alexa', alexaModel);