var unirest = require('unirest');

var alexaController = function (Alexa, AlexaResponse) {

    var handleRequest = function (req, res) {

        // call the Nedbank Alexa API
        unirest.post('http://www.yellowdog.co.za/nedbankalexa/api/alexa/intent')
        //unirest.post('http://localhost:54812/api/alexa/intent')
            .header('content-type', 'application/json')
            .send(req.body)
            .end(function (response) {
                //console.log(response);
                 res.status(200);
                 res.send(response.body);
            });

    }

    return {
        handleRequest: handleRequest
    }
}

module.exports = alexaController;
